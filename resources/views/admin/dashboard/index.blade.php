@extends('admin.layouts.app')

@section('content')
@include('admin.layouts.all-content-wrapper')
        
        @include('admin.layouts.header-advance')
        
        @include('admin.layouts.section-admin')
       
        @include('admin.layouts.product-sales')
       
        @include('admin.layouts.traffic-analysis')
        
        @include('admin.layouts.product-new-list')
        
        @include('admin.layouts.product-sales-area')
       
        @include('admin.layouts.author-area')
       
        @include('admin.layouts.calender')
        
        @include('admin.layouts.footer-copyright')
@endsection